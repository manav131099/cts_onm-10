rm(list = ls())
#errHandle = file('/home/admin/Logs/LogsIN001History.txt',open='w',encoding='UTF-8')
#sink(errHandle,type='message',append = T)
#sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN006Digest/Functions.R')
METERNICKNAMES = c("ABS","EMR","ER&D","SS","Unit 21","Unit 24")

x = 1

path = "/home/admin/Data/TORP Data/[IN-006T]"
pathgen1 = "/home/admin/Dropbox/Gen 1 Data/[IN-006T]"

if(!file.exists(path))
{
	dir.create(path)
}
if(!file.exists(pathgen1))
{
	dir.create(pathgen1)
}

yr = 2016
pathyr = paste(path,yr,sep="/")
pathyrgen1 = paste(pathgen1,yr,sep="/")

if(!file.exists(pathyr))
{
	dir.create(pathyr)
}
if(!file.exists(pathyrgen1))
{
	dir.create(pathyrgen1)
}

months = c("01","02","03","04","05","06","07","08","09","10","11","12")
daysmonths = c(31,28,31,30,31,30,31,31,30,31,30,31)

for(outer in 1:12)
{
	pathmonth = paste(pathyr,"/",yr,"-",months[outer],sep="")
	pathmonthgen1 = paste(pathyrgen1,"/",yr,"-",months[outer],sep="")
	pathmonthgen11 = paste(pathmonthgen1,METERNICKNAMES[1],sep="/")
	pathmonthgen12 = paste(pathmonthgen1,METERNICKNAMES[2],sep="/")
	pathmonthgen13 = paste(pathmonthgen1,METERNICKNAMES[3],sep="/")
	pathmonthgen14 = paste(pathmonthgen1,METERNICKNAMES[4],sep="/")
	pathmonthgen15 = paste(pathmonthgen1,METERNICKNAMES[5],sep="/")
	pathmonthgen16 = paste(pathmonthgen1,METERNICKNAMES[6],sep="/")
	
	if(!file.exists(pathmonth))
		dir.create(pathmonth)
	if(!file.exists(pathmonthgen1))
		dir.create(pathmonthgen1)
	if(!file.exists(pathmonthgen11))
	  dir.create(pathmonthgen11)
	if(!file.exists(pathmonthgen12))
	  dir.create(pathmonthgen12)
	if(!file.exists(pathmonthgen13))
	  dir.create(pathmonthgen13)
	if(!file.exists(pathmonthgen14))
	  dir.create(pathmonthgen14)
	if(!file.exists(pathmonthgen15))
	  dir.create(pathmonthgen15)
	if(!file.exists(pathmonthgen16))
	  dir.create(pathmonthgen16)
	x = 1
#	req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
	print('Logged in')
	while(x <= daysmonths[outer])
	{
  	day = x
#		if(outer == 12 && x == 25)
#		{
#			print('lt reached breaking')
#			break
#		}
  	if(x < 10)
  	{
   	 day = paste("0",x,sep="")
  	}
 		day1 = paste(day,"/",months[outer],"/",yr,sep="")
  	day2 = day1
  	day11 = paste("2016-",months[outer],"-",day,sep="")
#		df = fetchrawdata(day1,day2)
#  	write.table(df,file=paste(pathmonth,"/[IN-006T] ",day11,".txt",sep=""),row.names = F,
#              col.names = T,sep = "\t",append = F)
		df2 = cleansedata(paste(pathmonth,"/[IN-006T] ",day11,".txt",sep=""))
		
  	write.table(df2[[1]],file=paste(pathmonthgen11,"/[IN-006T-M1] ",day11,".txt",sep=""),row.names = F,
              col.names = T,sep = "\t",append = F)
  	write.table(df2[[2]],file=paste(pathmonthgen12,"/[IN-006T-M2] ",day11,".txt",sep=""),row.names = F,
  	            col.names = T,sep = "\t",append = F)
  	write.table(df2[[3]],file=paste(pathmonthgen13,"/[IN-006T-M3] ",day11,".txt",sep=""),row.names = F,
  	            col.names = T,sep = "\t",append = F)
  	write.table(df2[[4]],file=paste(pathmonthgen14,"/[IN-006T-M4] ",day11,".txt",sep=""),row.names = F,
  	            col.names = T,sep = "\t",append = F)
  	write.table(df2[[5]],file=paste(pathmonthgen15,"/[IN-006T-M5] ",day11,".txt",sep=""),row.names = F,
  	            col.names = T,sep = "\t",append = F)
  	write.table(df2[[6]],file=paste(pathmonthgen16,"/[IN-006T-M6] ",day11,".txt",sep=""),row.names = F,
  	            col.names = T,sep = "\t",append = F)
		print(paste(x,'done'))
  	x = x + 1
	}
}
