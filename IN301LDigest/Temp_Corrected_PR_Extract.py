import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast
import pyodbc

path='/home/admin/Dropbox/Gen 1 Data/[IN-301L]/'
delta=-.0041
capacity=31501.27
date=sys.argv[1]
break_date=(datetime.datetime.strptime(date,"%Y-%m-%d")+datetime.timedelta(days=1)).strftime("%Y-%m-%d")

final_df=pd.DataFrame(columns=['Date','GHI','PR','DA'])
def calc(date):
    df_wms=pd.read_csv(path+date[0:4]+'/'+date[0:7]+'/WMS_2_ICR 02 WMS (12) Pry/'+"[IN-301L]-WMS2-"+date+'.txt',sep='\t')
    df_mfm=pd.read_csv(path+date[0:4]+'/'+date[0:7]+'/MFM_2_MCR TVM 1 TRAFO/'+"[IN-301L]-MFM2-"+date+'.txt',sep='\t')
    df_wms['ts']=pd.to_datetime(df_wms['ts'])
    mask = (df_wms['ts'].dt.hour > 5) & (df_wms['ts'].dt.hour < 18)
    df_wms=df_wms[mask]
    df_mfm['ts']=pd.to_datetime(df_mfm['ts'])
    mask = (df_mfm['ts'].dt.hour > 5) & (df_mfm['ts'].dt.hour < 18)
    df_mfm=df_mfm[mask]
    df_wms=df_wms[['ts','POAI_avg','TmpBOM_avg']]
    df_mfm=df_mfm[['ts','W_avg']]
    df_merge=pd.merge(df_wms,df_mfm, on=['ts'],sort=False,how='left')
    tot_power=df_merge['W_avg']/1000
    tot_energy=tot_power.sum()/12
    ghi=df_merge['POAI_avg'].sum()/12000
    t_cell=df_merge['TmpBOM_avg']+3*(df_merge['POAI_avg']/1000)
    tot_power_corr=tot_power*(1+delta*(25-t_cell))
    tot_energy_corr=tot_power_corr.sum()/12
    pr=((tot_energy/capacity)/ghi)*100
    pr_corr=((tot_energy_corr/capacity)/ghi)*100
    df_final = pd.DataFrame({'Date':[date],'GHI':[ghi],'PR':[pr_corr],'DA':[round((len(df_mfm.dropna())/1.44),2)]},columns =['Date','GHI','PR','DA'])
    return df_final

for i in sorted(os.listdir(path)):
    for j in sorted(os.listdir(path+i)):
        for k in sorted(os.listdir(path+i+'/'+j)):
            if(k=='MFM_2_MCR TVM 1 TRAFO'):
                for l in sorted(os.listdir(path+i+'/'+j+'/'+k)):
                    temp_df=calc(l[15:-4])
                    if(l[15:-4]==break_date):
                        break
                    final_df=final_df.append(temp_df,sort=False)
                    
final_df.to_csv('/home/admin/Graphs/Graph_Extract/IN-301/[IN-301] Graph '+date+ " - Temperature Corrected PR Evolution.txt",sep='\t',index=False)





