rm(list = ls())
require('gtools')
args <- commandArgs(TRUE)
colNam = as.character(args[1])
colNam = trimws(colNam)
colNam = unlist(strsplit(colNam,""))
colNam = unlist(rev(colNam))
colNam = paste(colNam,collapse="")
colNam = toupper(colNam)

len = nchar(colNam)
final = 0
for(x in 1 : len)
{
	ascii = asc(substr(colNam,x,x)) - 64
	fac = max((x-1)*26,1)
	final = final + (ascii * fac)
}
print(final)
