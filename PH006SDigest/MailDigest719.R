rm(list = ls())
errHandle = file('/home/admin/Logs/LogsPH006SMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
system('rm -R "/home/admin/Dropbox/Second Gen/[PH-006S]"')
source('/home/admin/CODE/PH006SDigest/runHistory719.R')
require('mailR')
print('History done')
source('/home/admin/CODE/PH006SDigest/3rdGenData719.R')
print('3rd gen data called')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/PH006SDigest/aggregateInfo.R')

timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

preparebody = function(path)
{
  print("Enter Body Func")
	body = paste(body,"Site Name: Gaisano Capital, Mactan\n",sep="")
	body = paste(body,"\n Location: Cebu, Philippines\n")
	body = paste(body,"\n O&M Code: PH-006\n")
	body = paste(body,"\n System Size: 300.8 kWp\n")
	body = paste(body,"\n Number of Energy Meters: 1\n")
	body = paste(body,"\n Module Brand / Model / Nos: JA Solar / JAP6 / 940\n")
	body = paste(body,"\n Inverter Brand / Model / Nos: SMA / STP 60-10 / 4\n")
	body = paste(body,"\n Site COD: 2017-10-27\n")
	body = paste(body,"\n System age [days]:",daysactive,"\n")
	body = paste(body,"\n System age [years]:",round(daysactive/365,2))
  for(x in 1 : length(path))
  {
		print(path[x])
		body = paste(body,"\n\n_____________________________________________\n")
		days = unlist(strsplit(path[x],"/"))
		days = substr(days[length(days)],11,20)
		body = paste(body,days)
		body  = paste(body,"\n_____________________________________________\n")
		dataread = read.table(path[x],header = T,sep="\t")
		body = paste(body,"\nPts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
		body = paste(body,"\n\nDaily Irradiation [kWh/m2]:",as.character(dataread[1,3]))
		body = paste(body,"\n\nDaily Irradiation (Pyranometer) [kWh/m2]:",as.character(dataread[1,4]))
		body = paste(body,"\n\nEac-1 [kWh]:",as.character(dataread[1,18]))
		body = paste(body,"\n\nEac-2 [kWh]:",as.character(dataread[1,19]))
		body = paste(body,"\n\nYield-1 [kWh/kWp]:",as.character(dataread[1,22]))
		body = paste(body,"\n\nYield-2 [kWh/kWp]:",as.character(dataread[1,23]))
		body = paste(body,"\n\nPR-1 Si [%]:",as.character(dataread[1,20]))
		body = paste(body,"\n\nPR-2 Si [%]:",as.character(dataread[1,21]))
		body = paste(body,"\n\nPR-1 Pyr [%]:",as.character(dataread[1,26]))
		body = paste(body,"\n\nPR-2 Pyr [%]:",as.character(dataread[1,27]))
		body = paste(body,"\n\nMean Ambient Temp [C]:",as.character(dataread[1,5]))
		body = paste(body,"\n\nMean Ambient Temp Solar Hours [C]:",as.character(dataread[1,6]))
		body = paste(body,"\n\nMean Humidity [%]:",as.character(dataread[1,11]))
		body = paste(body,"\n\nMean Humidity Solar Hours [%]:",as.character(dataread[1,12]))
		body = paste(body,"\n\nLast recorded energy meter reading for the day [kWh]:",as.character(dataread[1,24]))
		body = paste(body,"\n\nLast recorded time-stamp for the day:",as.character(dataread[1,25]))
  }
print('Daily Summary for Body Done')
		body = paste(body,"\n\n_____________________________________________\n\n")
  body = paste(body,"Station History")
		body = paste(body,"\n\n_____________________________________________\n\n")

	body = paste(body,"Station Birth Date:",dobstation)
	body = paste(body,"\n\n# Days station active:",daysactive)
	body = paste(body,"\n\n# Years station active:",rf1((daysactive/365)))
print('3G Data Done')	
return(body)
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[719]"
pathwrite = "/home/admin/Dropbox/Second Gen/[PH-006S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[PH-006S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}
stnnickName = "719"
stnnickName2 = "PH-006S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
	yr = substr(lastdatemail,1,4)
	monyr = substr(lastdatemail,1,7)
	prevpathyear = paste(path,yr,sep="/")
	prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
	prevpathmonth = paste(prevpathyear,monyr,sep="/")
	currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
	prevwriteyears = paste(pathwrite,yr,sep="/")
	prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com"
uname <- "shravan.karthik@cleantechsolar.com" 
pwd = 'CTS&*(789'
recipients <- getRecipients("PH-006S","m")
#"jeeva.govindarasu@cleantechsolar.com")

wt =1
idxdiff = 1
while(1)
{
recipients <- getRecipients("PH-006S","m")
recordTimeMaster("PH-006S","Bot")
years = dir(path)
writeyears = paste(pathwrite,years[length(years)],sep="/")
checkdir(writeyears)
pathyear = paste(path,years[length(years)],sep="/")
months = dir(pathyear)
writemonths = paste(writeyears,months[length(months)],sep="/")
sumname = paste("[PH-006S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
checkdir(writemonths)
pathmonth = paste(pathyear,months[length(months)],sep="/")
tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
currday = dir(pathmonth)[length(dir(pathmonth))]
if(prevday == currday)
{
   if(wt == 1)
{
 print("")
 print("")
 print("__________________________________________________________")
 print(substr(currday,7,16))
 print("__________________________________________________________")
 print("")
 print("")
 print('Waiting')
wt = 0
}
{
  if(tm > 1319 || tm < 30)
  {
      Sys.sleep(120)
  }        
  else
  {
    Sys.sleep(3600)
  }
}
next
}
print('New Data found.. sleeping to ensure sync complete')
Sys.sleep(20)
print('Sleep Done')
idxdiff = match(currday,dir(pathmonth)) - match(prevday,dir(prevpathmonth))
if(idxdiff < 0)
{
	idxdiff = length(dir(prevpathmonth)) - match(prevday,dir(prevpathmonth)) + length(dir(pathmonth))
	newmonth = pathmonth
	newwritemonths = writemonths
	pathmonth = prevpathmonth
	writemonths = prevwritemonths
}
while(idxdiff)
{
{
if(prevday == dir(prevpathmonth)[length(dir(prevpathmonth))])
{
  pathmonth = newmonth
	writemonths = newwritemonths
  currday = dir(pathmonth)[1]
  monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
  strng = unlist(strsplit(months[length(months)],"-"))
  daystruemonth = nodays(strng[1],strng[2])
  dayssofarmonth = 0
}	
else
currday = dir(pathmonth)[(match(prevday,dir(pathmonth)) +1)]
}
dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t")
datawrite = prepareSumm(dataread)
datasum = rewriteSumm(datawrite)
currdayw = gsub("719","PH-006S",currday)
write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)



#monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
#monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
#monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
#globirr1 = globirr1 + as.numeric(datawrite[1,3])
#globirr2 = globirr2 + as.numeric(datawrite[1,4])
#globirr3 = globirr3 + as.numeric(datawrite[1,5])
#dayssofarmonth = dayssofarmonth + 1    
daysactive = daysactive + 1
#last30days[[last30daysidx]] = as.numeric(datawrite[1,4])
#last30days2[[last30daysidx]] = as.numeric(datawrite[1,5])



print('Digest Written');
{
  if(!file.exists(paste(writemonths,sumname,sep="/")))
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    print('Summary file created')
}
  else 
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
   print('Summary file updated')  
}
}
filetosendpath = c(paste(writemonths,currdayw,sep="/"))
filename = c(currday)
filedesc = c("Daily Digest")
dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t")
datawrite = prepareSumm(dataread)
prevdayw = gsub("719","PH-006S",prevday)
dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t")
print('Summary read')
if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
{
print('Difference in old file noted')

  datasum = rewriteSumm(datawrite)
  write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)


datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t")
  rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
  print(paste('Match found at row no',rn))
  datarw[rn,] = datasum[1,]
  write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
  filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
  filename[2] = prevday
  filedesc[2] = c("Updated Digest")
print('Updated old files.. both digest and summary file')
}

print('Sending mail')
body = ""
body = preparebody(filetosendpath)
body = gsub("\n ","\n",body)
send.mail(from = sender,
          to = recipients,
          subject = paste("Station [PH-006S] Digest",substr(currday,7,16)),
          body = body,
          smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
          authenticate = TRUE,
          send = TRUE,
          attach.files = filetosendpath,
          file.names = filename, # optional parameter
          file.descriptions = filedesc, # optional parameter
          debug = F)
print('Mail Sent');
recordTimeMaster("PH-006S","Mail",substr(currday,7,16))
prevday = currday
prevpathyear = pathyear
prevpathmonth = pathmonth
prevwriteyears = writeyears
prevwritemonths = writemonths
prevsumname = sumname
idxdiff = idxdiff - 1;
wt = 1
}
gc()
}
sink()
