rm(list = ls())

stnNam = "[KH-001S]"
path = paste("/home/admin/Dropbox/Second Gen/",stnNam,sep="")

yrs = dir(path)

pathwrite = paste("/home/admin/Graphs/YearSumm/",stnNam,"/",sep="")
if(!file.exists(pathwrite))
	dir.create(pathwrite)

for( x in 1 : length(yrs))
{
	pathwritefinal = paste(pathwrite,stnNam," ",yrs[x],".txt",sep="")
	start = 1 
	pathmons = paste(path,yrs[x],sep="/")
	mons = dir(pathmons)
	for( y in 1 : length(mons))
	{
		pathdays = paste(pathmons,mons[y],sep="/")
		days = dir(pathdays)
		pathread = paste(pathdays,days[1],sep="/")
		print(pathread)
		data = read.table(pathread,header=T,sep="\t",stringsAsFactors=F)
		{
			if(start)
			{
				write.table(data,file=pathwritefinal,sep="\t",row.names=F,col.names=T,append=F)
				start=0
			}
			else
				write.table(data,file=pathwritefinal,sep="\t",row.names=F,col.names=F,append=T)
		}
	}
}
