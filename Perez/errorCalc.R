rm(list =ls())

path = '/home/admin/Dropbox/GIS/Gen-1'
errHandle = file('/home/admin/Logs/LogsPerezError.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

stations = dir(path)

THRESHOLD = 300

filewritepath = paste('/home/admin/Dropbox/GIS/faulty_',THRESHOLD,".txt",sep="")
stnname = c()
tm = c()
ghi = c()
gti = c()
tiltwrite = c()
azi = c()
gmod = c()
zen = c()
inci = c()
skyb = c()
lat = c()
lon = c()
diffw = c()
index = 1

for(x in  1:length(stations))
{
	pathstation = paste(path,stations[x],sep="/")
	STNNAME = stations[x]
	years = dir(pathstation)
	for(y in 1 : length(years))
	{
		pathmonths = paste(pathstation,years[y],sep="/")
		months = dir(pathmonths)
		for(z in 1 : length(months))
		{
			pathtilts = paste(pathmonths,months[z],sep="/")
			tilts = dir(pathtilts)
			for(t in 1 : length(tilts))
			{
				TILTW = tilts[t]
				pathdays = paste(pathtilts,tilts[t],sep="/")
				days = dir(pathdays)
				for(u in 1 : length(days))
				{
					pathfile = paste(pathdays,days[u],sep="/")
					dataread = read.table(pathfile,header = T,sep="\t",stringsAsFactors=F)
					if(ncol(dataread) < 11)
					{
						print(paste('error less than 11 cols',days[u]))
						next
					}
					c1 = as.character(dataread[,1])
					c2 = as.numeric(dataread[,2])
					c3 = as.numeric(dataread[,4])
					c4 = as.numeric(dataread[,6])
					c5 = as.numeric(dataread[,7])
					c6 = as.numeric(dataread[,8])
					c7 = as.numeric(dataread[,9])
					c8 = as.numeric(dataread[,10])
					c9 = as.numeric(dataread[,11])
					if(ncol(dataread)>11)
					{
						c10 = as.numeric(dataread[,12])
						c11 = as.numeric(dataread[,13])
					}
					for(a in 1 : nrow(dataread))
					{
						diff = abs(c3[a]-c6[a])
						if(is.finite(diff) && diff > THRESHOLD)
						{
							tm[index] = c1[a]
							ghi[index] = c2[a]
							gti[index]=  c3[a]
							tiltwrite[index] = c4[a]
							azi[index] = c5[a]
							gmod[index] = c6[a]
							zen[index] = c7[a]
							inci[index] = c8[a]
							skyb[index] = c9[a]
							lat[index] = NA
							lon[index] = NA
							if(ncol(dataread) > 11)
							{
								lat[index] = c10[a]
								lon[index] = c11[a]
							}
							diffw[index] = diff
							stnname[index] = STNNAME
							index = index + 1
						}
					}
				}
			}
			print(paste(STNNAME,months[z],"done"))
		}
	}
}

df = data.frame(Tm=tm,Name=stnname,AbsDiff=diffw,GHI=ghi,GTI=gti,Tilt=tiltwrite,Azi=azi,
Gmod=gmod,Zen=zen,Inci=inci,SkyB=skyb,Lat=lat,Lon=lon,stringsAsFactors=F)
write.table(df,file=filewritepath,sep="\t",row.names=F,col.names=T,append=F)
sink()
