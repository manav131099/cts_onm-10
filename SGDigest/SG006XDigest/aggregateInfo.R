source('/home/admin/CODE/common/aggregate.R')

registerMeterList("SG-006X",c("Admin","Canteen","EM","Mill"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 9 #column for Yld-1
aggColTemplate[8] = 10 #column for Yld-2
aggColTemplate[9] = 15 #column for PR-1
aggColTemplate[10] = 16 #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "SG-003S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-006X","Admin",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 9 #column for Yld-1
aggColTemplate[8] = 10 #column for Yld-2
aggColTemplate[9] = 15 #column for PR-1
aggColTemplate[10] = 16 #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "SG-003S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-006X","Canteen",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 9 #column for Yld-1
aggColTemplate[8] = 10 #column for Yld-2
aggColTemplate[9] = 15 #column for PR-1
aggColTemplate[10] = 16 #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "SG-003S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-006X","EM",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 5 #Column no for DA
aggColTemplate[3] = 8 #column for LastRead
aggColTemplate[4] = 7 #column for LastTime
aggColTemplate[5] = 2 #column for Eac-1
aggColTemplate[6] = 3 #column for Eac-2
aggColTemplate[7] = 9 #column for Yld-1
aggColTemplate[8] = 10 #column for Yld-2
aggColTemplate[9] = 15 #column for PR-1
aggColTemplate[10] = 16 #column for PR-2
aggColTemplate[11] = 14 #column for Irr
aggColTemplate[12] = "SG-003S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("SG-006X","Mill",aggNameTemplate,aggColTemplate)
