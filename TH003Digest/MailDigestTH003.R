errHandle = file('/home/admin/Logs/LogsTH003Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/TH003Digest/HistoricalAnalysis2G3GTH003.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/TH003Digest/aggregateInfo.R')

initDigest = function(df,no)
{
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'MDB-01 Check'
		noac=""
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'MDB-01 Invoice'
		noac=""
	}
	else if(as.numeric(no)==3)
	{
		no2 = "MDB-02 Check"
		noac=""
	}
	else if(as.numeric(no)==4)
	{
		no2 = "MDB-02 Invoice"
		noac=""
	}
	}
	#ratspec = round(as.numeric(df[,2])/2624,2)
  body = "\n_________________________________________\n\n"
  body = paste(body,as.character(df[,1]),no2)
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Eac",noac,"-1 [kWh]: ",as.character(df[,2]),sep="")
	body = paste(body,"\n\nEac",noac,"-2 [kWh]: ",as.character(df[,3]),sep="")
	body = paste(body,"\n\nYield-1 [kWh/kWp]:",as.character(df[,6]))
	body = paste(body,"\n\nYield-2 [kWh/kWp]:",as.character(df[,7]))
	irrsrc = as.character(df[,14])
	body = paste(body,"\n\nTotal daily irradiation (from ",irrsrc,") [kWh/m2]: ",df[,10],sep="")
	body = paste(body,"\n\nPR-1 [%]:",as.character(df[,8]))
	body = paste(body,"\n\nPR-2 [%]:",as.character(df[,9]))
	#if(as.numeric(df[,11]) > 80)
	#{
	#		body = paste(body," -- WARNING SYSTEM COULD BE RUNNING AT FULL THROTTLE")
	#	}
	body = paste(body,"\n\nRatio [%]:",as.character(df[,11]))
	acpts = round(as.numeric(df[,4]) * 14.4)
	body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,4]),"%)",sep="")
	body = paste(body,"\n\nDowntime (%):",as.character(df[,5]))
	body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,12]))
	body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,13]),"\n")
  return(body)
}

printtsfaults = function(TS,num,body)
{
  no = num
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'MDB-01 Check'
		noac=""
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'MDB-01 Invoice'
		noac=""
	}
	else if(as.numeric(no)==3)
	{
		no2 = "MDB-02 Check"
		noac=""
	}
	else if(as.numeric(no)==4)
	{
		no2 = "MDB-02 Invoice"
		noac=""
	}
	}
	num = no2
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n_________________________________________\n\n")
		body = paste(body,paste("Timestamps for",num,"where Pac < 1 between 8am -5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
	}
	return(body)
}

sendMail = function(df1,df2,df3,df4,pth1,pth2,pth3,pth4,pth5,invoiceData)
{
	datepth = as.character(Sys.Date())                                                 
	invoicePath <- paste('/tmp/[TH-003X] TH Invoicing summary ',datepth,'.txt',sep="") 
 	filetosendpath = c(pth1,pth2,pth3,pth5,invoicePath)
  filetosendpath = c(pth1,pth2,pth3,pth4,pth5,invoicePath)

	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	
	yrcmd = substr(currday,14,17)
	moncmd = substr(currday,14,20)
	command = paste("Rscript '/home/admin/CODE/JasonGraphs/TH-003X/TH-003X Eac grouping.R'",yrcmd,moncmd)
	system(command) 
	invoiceData = read.table(invoicePath,header=T,sep="\t",stringsAsFactors=F)
	
	dateInv = as.character(invoiceData[,1])
	
	userow = nrow(invoiceData)
	idxmtch = match(substr(currday,14,23),dateInv)
	if(is.finite(idxmtch))
		userow = idxmtch
	invoiceData = invoiceData[userow,]
	
	data3G = read.table(pth5,header=T,sep="\t",stringsAsFactors=F)
	idxineed = match(substr(as.character(currday),14,23),as.character(data3G[,1]))
	cov1 = cov2 = sd1 = sd2 = NA
	if(is.finite(idxineed))
	{
		sd1 = as.character(data3G[idxineed,47])
		cov1 = as.character(data3G[idxineed,48])
		sd2 = as.character(data3G[idxineed,49])
		cov2 = as.character(data3G[idxineed,50])
	}
	print('Filenames Processed')
	body=""
	body = paste(body,"Site Name: Sheico Thailand",sep="")
	body = paste(body,"\n\nLocation: Rayong, Thailand")
	body = paste(body,"\n\nO&M Code: TH-003")
	body = paste(body,"\n\nSystem Size: 994.5 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 4")
	body = paste(body,"\n\nModule Brand / Model / Nos: Jinko / JKM325PP-325Wp / 3060")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP60-10 / 13")
	body = paste(body,"\n\nSite COD: 2019-01-11")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE))/365,2)))
 
	body = paste(body,"\n\nPeak Eac1-M1 [kWh]:",as.character(invoiceData[,6]))
	body = paste(body,"\n\nOffPeak Eac1-M1 [kWh]:",as.character(invoiceData[,7]))
	body = paste(body,"\n\nPeak Eac2-M1 [%]:",as.character(round(invoiceData[,8]),2))
	body = paste(body,"\n\nOffPeak Eac2-M1 [%]:",as.character(invoiceData[,9]))
	body = paste(body,"\n\nPeak Eac1-M2 [kWh]:",as.character(invoiceData[,15]))
	body = paste(body,"\n\nOffPeak Eac1-M2 [kWh]:",as.character(invoiceData[,16]))
	body = paste(body,"\n\nPeak Eac2-M2 [%]:",as.character(invoiceData[,17]))
	body = paste(body,"\n\nOffPeak Eac2-M2 [%]:",as.character(invoiceData[,18]))
	body = paste(body,"\n\nDay:",as.character(invoiceData[,2]))
	body = paste(body,"\n\nHoliday [Y/N]:",as.character(invoiceData[,3]))
	body = paste(body,"\n\nSD/COV Invoice [%]:",sd1,"/",cov1)
	body = paste(body,"\n\nSD/COV Check [%]:",sd2,"/",cov2)
  
	body = paste(body,initDigest(df2,2))  #its correct, dont change
	body = paste(body,initDigest(df4,4),sep="\n")  #its correct, dont change
	body = paste(body,initDigest(df1,1))  #its correct, dont change
	body = paste(body,initDigest(df3,3),sep="\n")  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,1,body)
  body = printtsfaults(TIMESTAMPSALARM2,2,body)
  body = printtsfaults(TIMESTAMPSALARM3,3,body)
  body = printtsfaults(TIMESTAMPSALARM4,4,body)
	print('2G data processed')
	body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n_________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	print('3G data processed')
	body = gsub("\n ","\n",body)
	while(1)
	{

  graph_command=paste("Rscript /home/admin/CODE/TH003Digest/TH-003X_PR_Plot.R", substr(currday,14,23), sep=" ")
  system(graph_command,wait=TRUE)	
  graph_path=paste("/home/admin/Graphs/Graph_Output/TH-003/[TH-003] Graph ", substr(currday,14,23), " - PR Evolution.pdf", sep="")	
  graph_extract_path=paste("/home/admin/Graphs/Graph_Extract/TH-003/[TH-003] Graph ", substr(currday,14,23), " - PR Evolution.txt", sep="")

  graph_command=paste("python3 /home/admin/CODE/TH003Digest/Meter_CoV_Graph.py",substr(currday,14,23),sep=" ")
  system(graph_command,wait=TRUE)
  graph_path2=paste('/home/admin/Graphs/Graph_Output/TH-003/[TH-003] Graph ',substr(currday,14,23),' - Meter CoV.pdf',sep="")
  graph_extract_path2=paste('/home/admin/Graphs/Graph_Extract/TH-003/[TH-003] Graph ',substr(currday,14,23),' - Meter CoV.txt',sep="")
  
  filestosendpath = c(graph_path,graph_extract_path,graph_path2,graph_extract_path2,filetosendpath)
  firecond = lastMailDate(paste('/home/admin/Start/MasterMail/TH-003X_Mail.txt',sep=""))
  if (currday == firecond)
    return()
  mailSuccess = try(send.mail(from = sender,
            to = recipients,
            subject = paste("Station [TH-003X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filestosendpath,
            #file.names = filenams, # optional parameter
            debug = F),silent=T)
	if(class(mailSuccess)=='try-error')
	{
		Sys.sleep(180)
		next
	}
	break
	}
	recordTimeMaster("TH-003X","Mail",substr(currday,14,23))
}

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("TH-003X","m")

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	recipients = getRecipients("TH-003X","m")
  recordTimeMaster("TH-003X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[TH-003X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
			#if(length(stations) > 2)
			#stations = c(stations[2],stations[3],stations[1])
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      pathdays3 = paste(pathmonths,stations[3],sep="/")
      pathdays4 = paste(pathmonths,stations[4],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
      writepath2Gdays4 = paste(writepath2Gmon,stations[4],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      checkdir(writepath2Gdays3)
      checkdir(writepath2Gdays4)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      days3 = dir(pathdays3)
      days4 = dir(pathdays4)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
			if(length(days) != length(days2) || length(days2)!=length(days3) || length(days3)!=length(days) || length(days4) != length(days))
			{
				daycnt = min(length(days),length(days2),length(days3),length(days4))
				days = tail(days,daycnt)
				days2 = tail(days2,daycnt)
				days3 = tail(days3,daycnt)
				days4 = tail(days4,daycnt)
			}
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
         }
				 if(grepl("Copy",c(days[t],days2[t],days3[t],days4[t])))
				 {
				 	daycop = c(days[t],days2[t],days3[t],days4[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]) && is.na(days2[t]) && is.na(days3[t]) && is.na(days4[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
				 if(!(condn1 || condn2))
				 {
				 	sendmail = 0
				 }
				 else
				 {
         	DAYSALIVE = DAYSALIVE + 1
				 }
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
				 print(paste('Processing',days3[t]))
				 print(paste('Processing',days4[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
         writepath2Gfinal4 = paste(writepath2Gdays4,"/",days4[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
         readpath3 = paste(pathdays3,days3[t],sep="/")
         readpath4 = paste(pathdays4,days4[t],sep="/")
				 TOTSOLAR <<- 0
				 METERCALLED <<- 1 # its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 2  #its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				 METERCALLED <<- 3 # its correct dont change
         df3 = secondGenData(readpath3,writepath2Gfinal3)
				 METERCALLED <<- 4 # its correct dont change
         df4 = secondGenData(readpath4,writepath2Gfinal4)
				 invoiceData = computeInvoice(readpath2,readpath4)
				thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath2Gfinal4,writepath3Gfinal,invoiceData)
  if(sendmail ==0)
  {
    Sys.sleep(3600)
  }
	print('Sending Mail')
  sendMail(df1,df2,df3,df4,writepath2Gfinal,writepath2Gfinal2,
	writepath2Gfinal3,writepath2Gfinal4,writepath3Gfinal,invoiceData)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
